﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_11_12_2019
{
    public static class DbManager
    {
        public static List<Product> GetAllProductsByCategory(string category)
        {
            using (var context = new AppContext())
            {
                return context.Products.Where(product => product.Category.Name == category).ToList();
            }
        }

        public static void ChangeProduct(Product product)
        {
            using (var context = new AppContext())
            {
                var toChange = context.Products.Where(p => p.Id == product.Id).FirstOrDefault();
                if (toChange != null)
                {
                    toChange = product;
                    context.SaveChanges();
                }
            }
        }

        public static void ChangeCategory(Category category)
        {
            using (var context = new AppContext())
            {
                var toChange = context.Categories.Where(c => c.Id == category.Id).FirstOrDefault();
                if(toChange != null)
                {
                    toChange = category;
                    context.SaveChanges();
                }
            }
        }

        public static List<Category> GetAllCategories(string n = null)
        {
            using (var context = new AppContext())
            {
                return context.Categories.ToList();
            }
        }
        public static void AddCategory(Category category)
        {
            using (var context = new AppContext())
            {
                context.Categories.Add(category);
                context.SaveChanges();
            }
        }
        public static void AddProduct(Product product)
        {
            using(var context = new AppContext())
            {
                context.Categories.Where(x => product.Category.Name == x.Name)
                                  .FirstOrDefault()
                                  .Products
                                  .Add(product);
            }
        }
    }
}
