﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//Мастер спорта по костылям
namespace Practice_11_12_2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Product> products;
        private List<Category> categories;
        private Func<string, ICollection> dbAction;

        private void Zaglushka()
        {
            var chem = new Category
            {
                Name = "chemistry",
            };
            var chems = new List<Product> { new Product
                {
                    Name = "wish",
                    Price = 123.213
                },
                new Product {
                    Name = "cast",
                    Price = 1.11
                } };
            var food = new Category
            {
                Name = "food",
            };
            var foods = new List<Product>
                { new Product
                {
                    Name = "hotdog",
                    Price = 200
                },
                new Product
                {
                    Name = "sosiska",
                    Price = 1230
                }
                };
            foreach (var f in foods)
            {
                food.Products.Add(f);
            }
            foreach (var ch in chems)
            {
                chem.Products.Add(ch);
            }

            DbManager.AddCategory(chem);
            DbManager.AddCategory(food);
        }

        

        public MainWindow()
        {
            InitializeComponent();
            Zaglushka();
            products = new List<Product>();
            categories = new List<Category>();

            RefreshCategoriesComboBox();           
        }
        private void RefreshCategoriesComboBox()
        {
            //categoriesComboBox.ItemsS.Clear();
            dbAction = new Func<string, ICollection>(DbManager.GetAllCategories);
            dbAction.BeginInvoke(null, ProcessResult, null);
        }

        private void CreateButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void ListViewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = productsListView.SelectedItem as Product;
            if(item != null)
            {
                var changeWindow = new ChangeWindow(item, false);
   
                if(changeWindow.ShowDialog() == true)
                {
                    DbManager.ChangeProduct(changeWindow.Result as Product);
                    RefreshCategoriesComboBox();
                    RefreshProductsListView();
                }
            }      
        }

        private void CategoryComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = ((Category)categoriesComboBox.SelectedItem);
            if (item != null)
            {
                dbAction = new Func<string, ICollection>(DbManager.GetAllProductsByCategory);
                dbAction.BeginInvoke(item.Name, ProcessResult, null);
            }
        }
        private void RefreshProductsListView()
        {
            var item = ((Category)categoriesComboBox.SelectedItem);
            if (item != null)
            {
                dbAction = new Func<string, ICollection>(DbManager.GetAllProductsByCategory);
                dbAction.BeginInvoke(item.Name, ProcessResult, null);
            }
        }

        private void ProcessResult(IAsyncResult result)
        {
            var end = dbAction.EndInvoke(result);

            if (end is ICollection<Category>)
            {
                categories = end.OfType<Category>().ToList();
                Dispatcher.Invoke(() => categoriesComboBox.ItemsSource = categories);
            }
            else if (end is ICollection<Product>)
            {
                products = end.OfType<Product>().ToList();
                Dispatcher.Invoke(() => productsListView.ItemsSource = products);
            }
        }

        private void ChangeButtonClick(object sender, RoutedEventArgs e)
        {

        }
    }
}
