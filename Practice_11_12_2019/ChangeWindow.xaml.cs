﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Practice_11_12_2019
{
    /// <summary>
    /// Логика взаимодействия для ChangeWindow.xaml
    /// </summary>
    public partial class ChangeWindow : Window
    {
        private const int DEFAULT_TEXTBOX_WIDTH = 150;
        private const int DEFAULT_TEXTBOX_HEIGHT = 25;

        public object Result { get; set; }
        TextBox nameBox;
        TextBox priceBox;
        Label nameLabel;
        Label priceLabel;

        public ChangeWindow(object data, bool newMatter)
        {
            InitializeComponent();
            Result = data;
            
            if (data is Product)
            {
                InitAsProduct();
            }
        }

        private void InitAsCategory()
        { 

        }

        private void InitAsProduct()
        {
            Product product = Result as Product;
            nameLabel = LabelFactoty("Название:", 0, 20);
            nameBox = TextBoxFactory(product.Name, 100, 20);
            priceLabel = LabelFactoty("Цена:", 0, 60);
            priceBox = TextBoxFactory(product.Price.ToString(), 100, 60);

            grid.Children.Clear();
            grid.Children.Add(nameLabel);
            grid.Children.Add(nameBox);
            grid.Children.Add(priceLabel);
            grid.Children.Add(priceBox);
            grid.Children.Add(confirmButton);
        }
        private Label LabelFactoty(string content, int marginX, int marginY)
        {
            Label result = new Label();
            result.Content = content;
            result.Margin = new Thickness(marginX, marginY, 0, 0);

            return result;
        }
        private TextBox TextBoxFactory(string text, int marginX, int marginY)
        {
            TextBox result = new TextBox();
            result.Text = text;
            result.Width = DEFAULT_TEXTBOX_WIDTH;
            result.Height = DEFAULT_TEXTBOX_HEIGHT;
            result.Margin = new Thickness(marginX, marginY, 0, 0);
            result.VerticalAlignment = VerticalAlignment.Top;
            result.HorizontalAlignment = HorizontalAlignment.Left;

            return result;
        }

        private void ConfirmButtonClick(object sender, RoutedEventArgs e)
        {
            if (Result is Product)
            {
                if(!double.TryParse(priceBox.Text, out double price))               
                    return;

                Product product = new Product();
                product.Name = nameBox.Text;
                product.Price = price;
                product.Id = (Result as Product).Id;
                product.Category = (Result as Product).Category;

                Result = product;
                this.DialogResult = true;
            }
            else
            {
                MessageBox.Show("ASDASD");
            }
        }

        
    }
}
