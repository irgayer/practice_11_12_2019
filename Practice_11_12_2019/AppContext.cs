﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_11_12_2019
{
    public class AppContext : DbContext
    {
        public AppContext() : base("DbConnection") 
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<AppContext>());
        }


        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
